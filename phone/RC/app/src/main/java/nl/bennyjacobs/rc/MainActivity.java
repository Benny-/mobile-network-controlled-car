package nl.bennyjacobs.rc;

import java.io.EOFException;
import java.io.IOException;

import nl.bennyjacobs.aapbridge.bridge.AccessoryBridge;
import nl.bennyjacobs.aapbridge.aap.UsbConnection;

import android.annotation.TargetApi;
import android.util.Log;
import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Toast;

public class MainActivity extends Activity {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static AccessoryBridge staticBridge;

    private WebView webview;

    private AccessoryBridge bridge;
    private WebAppInterface webAppInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        if( staticBridge == null || !staticBridge.isOpen())
        {
            try {
                bridge = new AccessoryBridge(UsbConnection.easyConnect(getApplicationContext()));
                staticBridge = bridge;
            } catch (IOException e) {
                String msg = "Could not setup aapbridge: "+e.getLocalizedMessage();
                Log.e(TAG, msg, e);
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        else
        {
            bridge = staticBridge;
        }

        webview = (WebView) this.findViewById(R.id.webView);
        webview.getSettings().setJavaScriptEnabled(true);
        try {
            webAppInterface = new WebAppInterface(this, bridge);
        } catch (Exception e) {
            String msg = "Could not setup WebAppInterface: "+e.getLocalizedMessage();
            Log.e(TAG, msg, e);
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            finish();
        }
        webview.addJavascriptInterface(webAppInterface, "Motor");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE))
            { WebView.setWebContentsDebuggingEnabled(true); }
        }

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptThirdPartyCookies(webview, true);

        webview.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.d(TAG, "onPermissionRequest");
                MainActivity.this.runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void run() {
                        request.grant(request.getResources());
                    }
                });
            }

        });

        webview.loadUrl("https://bennyjacobs.nl/rc/car.html");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.reload) {
            webview.loadUrl("https://bennyjacobs.nl/rc/car.html");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        webview.destroy();
    }
}

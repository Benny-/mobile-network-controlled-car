package nl.bennyjacobs.rc;

import android.content.Context;
import android.os.Message;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.io.IOException;

import nl.bennyjacobs.aapbridge.bridge.AccessoryBridge;
import nl.bennyjacobs.aapbridge.bridge.ServiceRequestException;
import nl.bennyjacobs.aapbridge.dbus.DbusHandler;
import nl.bennyjacobs.aapbridge.dbus.DbusMethods;

/**
 * Created by benny on 19-11-15.
 */
public class WebAppInterface {
    private Context mContext;
    private DbusMethods dbusMethods;
    private DbusHandler dbusHandler;

    public static final String TAG = "WebAppInterface";

    /** Instantiate the interface and set the context */
    WebAppInterface(Context context, AccessoryBridge bridge) throws IOException, ServiceRequestException{
        mContext = context;
        dbusMethods = new DbusMethods(bridge);
        dbusMethods.setBusname("nl.bennyjacobs.rc.car");
        dbusMethods.setObjectpath("/");
        dbusMethods.setInterfaceName("nl.bennyjacobs.rc.car");
        dbusHandler = new DbusHandler() {
            @Override
            public void handleMessage(Message msg) {
                Log.i(TAG, msg.toString());
            }
        };
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void move(int value) throws IOException {
        Log.v(TAG, "move("+value+")");
        dbusMethods.methodCall(dbusMethods.getBusname(), dbusMethods.getObjectpath(), dbusMethods.getInterfaceName(), "move", value);
    }

    @JavascriptInterface
    public void turn(int value) throws IOException {
        Log.v(TAG, "turn("+value+")");
        dbusMethods.methodCall(dbusMethods.getBusname(),dbusMethods.getObjectpath(),dbusMethods.getInterfaceName(), "turn", value);
    }

    @JavascriptInterface
    public void stop() throws IOException {
        Log.v(TAG, "stop()");
        dbusMethods.methodCall(dbusMethods.getBusname(),dbusMethods.getObjectpath(),dbusMethods.getInterfaceName(), "stop");
    }
}

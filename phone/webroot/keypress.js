
// Delay in ms
function keypress(delay, keycode, fn) {

    var down = false;
    var intervalID = null;
    
    var callFunction = function()
    {
        if(down)
        {
            fn()
        }
        else
        {
            clearInterval(intervalID)
        }
    }

    $('body').on('keydown',function(event) {
        if (event.keyCode == keycode && !down)
        {
            event.preventDefault();
            down = true
            intervalID = window.setInterval(callFunction, delay);
            fn()
        }
    })

    $('body').on('keyup',function(event) {
        if (event.keyCode == keycode)
        {
            clearInterval(intervalID)
            down = false
            event.preventDefault();
        }
    })
}


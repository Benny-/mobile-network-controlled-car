
var speed = 15;

var webrtc = new SimpleWebRTC({
    localVideoEl: '',
    remoteVideosEl: '',

    autoRequestMedia: true,

    receiveMedia: {
        mandatory: {
            OfferToReceiveAudio: false,
            OfferToReceiveVideo: false
        }
    },
});

webrtc.on('readyToCall', function () {
    console.log('readyToCall');
    webrtc.joinRoom('ThePitstop');
});

webrtc.on('createdPeer', function (peer) {
    console.log('createdPeer', peer);
});

webrtc.on('videoAdded', function (video, peer) {
    console.log('videoAdded', peer.nick);
});

// local p2p/ice failure
webrtc.on('iceFailed', function (peer) {
    var pc = peer.pc;
    console.log('had local relay candidate', pc.hadLocalRelayCandidate);
    console.log('had remote relay candidate', pc.hadRemoteRelayCandidate);
});

// remote p2p/ice failure
webrtc.on('connectivityError', function (peer) {
    var pc = peer.pc;
    console.log('had local relay candidate', pc.hadLocalRelayCandidate);
    console.log('had remote relay candidate', pc.hadRemoteRelayCandidate);
});

webrtc.on('channelMessage', function(peer, channel, data) {
    var message = data.type;
    var payload = data.payload;

    if(channel == 'movement')
    {
        console.log('Received movement:', message, payload);
        
        // 'Motor' is a api provided by the webview.
        
        if(message == 'speed')
        {
            speed = parseInt(payload, 10)
        }
        
        if(message == 'keys')
        {
            if(payload == 'up')
                Motor.move(speed)
            if(payload == 'left')
                Motor.turn(19)
            if(payload == 'middle')
                Motor.stop()
            if(payload == 'right')
                Motor.turn(-19)
            if(payload == 'down')
                Motor.move(-speed)
        }
        
        if(message == 'gps')
        {
            // TODO: GPS nav.
        }
    }
    if(channel == 'sound')
    {
        if(message == 'howl')
        {
            var sound = new Howl({
              urls: payload.split(','),
              volume: 0.8,
              onloaderror: function(e) {
                    console.error(e.message);
                },
            }).play()
        }
    }
});


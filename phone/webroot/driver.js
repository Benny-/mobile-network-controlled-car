
var watchingCars = $('#watchingCars');
var speedSlider = $('#speedSlider');
var playRemotely = $('#playRemotely');
var playLocally = $('#playLocally');
var audioUrlBox = $('#audioUrlBox');

var button_ids = ['button_up','button_left','button_middle','button_right','button_down']

var webrtc = new SimpleWebRTC({
    // the id/element dom element that will hold "our" video
    localVideoEl: '',
    // the id/element dom element that will hold remote videos
    remoteVideosEl: '',
    // immediately ask for camera access
    autoRequestMedia: true,
});

button_ids.forEach(function(id) {
    var button = $('#'+id);
    button.mousehold(200, function() {
        console.log('Button clicked',id)
        webrtc.sendDirectlyToAll('movement','keys','middle'); // Reset direction and speed.
        webrtc.sendDirectlyToAll('movement','keys',id.split("_")[1]);
    })
});

speedSlider.on('change', function(){
    webrtc.sendDirectlyToAll('movement','speed',speedSlider.val());
});

playLocally.click(function(e){
    var sound = new Howl({
      urls: audioUrlBox.val().split(','),
      volume: 0.8,
      onloaderror: function(e) {
            alert(e.message);
        },
    }).play()
})

playRemotely.click(function(e){
    webrtc.sendDirectlyToAll('sound','howl', audioUrlBox.val());
})

$('#button_upleft').mousehold(200, function() {
    console.log('Button clicked','button_upleft')
    webrtc.sendDirectlyToAll('movement','keys','up');
    webrtc.sendDirectlyToAll('movement','keys','left');
})

$('#button_upright').mousehold(200, function() {
    console.log('Button clicked','button_upright')
    webrtc.sendDirectlyToAll('movement','keys','up');
    webrtc.sendDirectlyToAll('movement','keys','right');
})

$('#button_downleft').mousehold(200, function() {
    console.log('Button clicked','button_downleft')
    webrtc.sendDirectlyToAll('movement','keys','down');
    webrtc.sendDirectlyToAll('movement','keys','left');
})

$('#button_downright').mousehold(200, function() {
    console.log('Button clicked','button_downright')
    webrtc.sendDirectlyToAll('movement','keys','down');
    webrtc.sendDirectlyToAll('movement','keys','right');
})

// Left arrow
keypress(300, 37, function(e){
    webrtc.sendDirectlyToAll('movement','keys','left');
})
$('body').on('keyup',function(event) {
    if (event.keyCode == 37)
        webrtc.sendDirectlyToAll('movement','keys','middle'); // Stop direction movement.
})

// Up arrow
keypress(300, 38, function(e){
    webrtc.sendDirectlyToAll('movement','keys','up');
})

// Right arrow
keypress(300, 39, function(e){
    webrtc.sendDirectlyToAll('movement','keys','right');
})
$('body').on('keyup',function(event) {
    if (event.keyCode == 39)
        webrtc.sendDirectlyToAll('movement','keys','middle'); // Stop direction movement.
})

// Down arrow
keypress(300, 40, function(e){
    webrtc.sendDirectlyToAll('movement','keys','down');
})

// ctrlKey to stop
keypress(300, 17, function(e){
    webrtc.sendDirectlyToAll('movement','keys','middle');
})

webrtc.on('readyToCall', function () {
  console.log('readyToCall');
  webrtc.joinRoom('ThePitstop');
});

webrtc.on('createdPeer', function (peer) {
    console.log('createdPeer', peer);
});

webrtc.on('videoAdded', function (video, peer) {
    console.log('videoAdded', video, peer);

    var videoContainer = document.createElement('div');
    videoContainer.className = 'videoContainer';
    videoContainer.id = 'videoContainer_' + webrtc.getDomId(peer) + '_' + video.id;
    videoContainer.appendChild(video);
    // suppress contextmenu
    video.oncontextmenu = function () { return false; };

    var carDiv = document.getElementById('carDiv_'+webrtc.getDomId(peer));
    
    if(carDiv == null)
    {
        carDiv = $('<div id="'+'carDiv_'+webrtc.getDomId(peer)+'" />')[0];
    }
    
    carDiv.appendChild(videoContainer);

    this.on('channelOpen', function (channel) {
        console.log('channelOpen', channel);
    });

    //webrtc.sendDirectlyToAll('movement','gps',"lon/lat");
    watchingCars.append(carDiv);
});

webrtc.on('videoRemoved', function (video, peer) {
    console.log('video removed ', video, peer);
    var carDiv = document.getElementById('carDiv_'+webrtc.getDomId(peer));
    carDiv.remove(video)
});

// local p2p/ice failure
webrtc.on('iceFailed', function (peer) {
    var pc = peer.pc;
    console.log('had local relay candidate', pc.hadLocalRelayCandidate);
    console.log('had remote relay candidate', pc.hadRemoteRelayCandidate);
});

// remote p2p/ice failure
webrtc.on('connectivityError', function (peer) {
    var pc = peer.pc;
    console.log('had local relay candidate', pc.hadLocalRelayCandidate);
    console.log('had remote relay candidate', pc.hadRemoteRelayCandidate);
});


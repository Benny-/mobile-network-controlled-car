#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys

import gobject
import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
# https://github.com/adafruit/adafruit-beaglebone-io-python
import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.UART as UART
import serial

DBusGMainLoop(set_as_default=True)
loop = gobject.MainLoop()
bus = dbus.SystemBus()
bus_name = dbus.service.BusName('nl.bennyjacobs.rc.car', bus)

UART.setup("UART2")

class Car(dbus.service.Object):

    """
    Control a Sabertooth motor driver from dbus.
    """

    def __init__(self, object_path="/"):
        self.ser = serial.Serial(port="/dev/ttyO2", baudrate=9600, bytesize=8, parity='N', stopbits=1, rtscts=0)
        self.ser.close()
        self.ser.open()
        if not self.ser.isOpen():
            raise AssertionError('Serial is not open')
        
        # Serial timeout.
        # The timeout scales 1 unit per 100ms of timeout, so a command of 10 would make a timeout of 1000ms
        # The serial timeout will prevent the motor driver from running without a head.
        self.sendPacketizedSerial(0b00001110,3)
        
        dbus.service.Object.__init__(self, bus_name, object_path)

    def sendPacketizedSerial(self, command, value):
        address = 128 # address is set using dip switches on the Sabertooth.
        checksum = (address + command + value) & 127
        print("Sending", address, command, value, checksum)
        self.ser.write(bytearray([address, command, value, checksum]))

    @dbus.service.method(dbus_interface='nl.bennyjacobs.rc.car', in_signature='i', out_signature='')
    def move(self, value):
        if (value > 127) or (value < -127):
            raise ValueError('Move value must be between -127 and 127')
        
        if value >= 0:
            self.sendPacketizedSerial(0b00000000, abs(value)) # Forward
        else:
            self.sendPacketizedSerial(0b00000001, abs(value)) # Back

    @dbus.service.method(dbus_interface='nl.bennyjacobs.rc.car', in_signature='i', out_signature='')
    def turn(self, value):
        if (value > 127) or (value < -127):
            raise ValueError('Move value must be between -127 and 127')
        
        if value >= 0:
            self.sendPacketizedSerial(0b00000100, abs(value)) # Left
        else:
            self.sendPacketizedSerial(0b00000101, abs(value)) # Right

    @dbus.service.method(dbus_interface='nl.bennyjacobs.rc.car', in_signature='', out_signature='')
    def stop(self):
        self.move(0)
        self.turn(0)

if __name__ == '__main__':
    car = Car()
    print('Starting event loop')
    loop.run()

